-- types: 
-- 1
-- 2
-- 3
-- 4
-- 5
-- 6
-- 7
function love.load()
    math.randomseed(os.time())
    player = {
        health = math.random(100, 200),
        type = math.random(1, 7),
        attacks = {
            {damage = math.random(10, 20), accuracy = math.random(1, 100)},
            {damage = math.random(10, 20), accuracy = math.random(1, 100)}
        }
    }
    message = 0
end

function love.draw()
    love.graphics.setFont(love.graphics.newFont(50))
    love.graphics.print(message)
end

function love.update(dt)
    if love.keyboard.isDown("right") or love.keyboard.isDown("up") then
        message = message + 1
    end
    if love.keyboard.isDown("left") or love.keyboard.isDown("down") then
        message = message - 1
    end
end
