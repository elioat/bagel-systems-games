# WikiJousting

WikiJousting is a competitive sport where 2 or more individuals race to reach a target page. Whomever reaches the target page in the fewest steps wins the joust.

Players competing in a Wikijoust (or just a "joust") are known as either players or as "knights."

## How to play

Visit [Wikipedia](https://en.wikipedia.org/wiki/Main_Page) (this document uses the English language version of Wikipedia as example, but there is no reason not to use another language locale). 

Designate 1 knight (perhaps the winner of the last bout) to select the target page. They can choose a specific page, or they can use the "[Random Article](https://en.wikipedia.org/wiki/Special:Random)" link to choose a target at random. 

Once the target page is decided all knights click the "[Random Article](https://en.wikipedia.org/wiki/Special:Random)" link -- from there, the knights progress through to the target. 

Once the target is reached all knights review their browser history's to count up the number of pages that were navigated through to reach the target page. 

## Example play

Sarah and Ricardo are the knights. 

Ricardo clicks "[Random Article](https://en.wikipedia.org/wiki/Special:Random)" to generate the target page. The resulting page is the entry for Joan of Arc. 

Ricardo and Sarah agree on the target page and each click the Random Article link again to start the joust.

Sarah's first page is Polish Village, from there they click Poland > Member States of the EU > France > Joan of Arc. 

Ricardo's first page is Songlines, from there they click Bruce Chatwin > West Riding Of Yorkshire > Southern England > EU Parliament Constitution > Member State of the EU > France > Joan of Arc. 

Sarah wins the joust!
