--[[
If too much seed has not been planted
	S <= 6 * L
And if there are enough farmers
	W >= L/4
then
	H = 4 * S


If too much seed has not been planted
	S <= 6 * L
but there are too few farmers
	W < L/4
then
	H = (4 * w/L) 4 * S


If too much seed has been planted 
	S > 6 * L
and if there are enough farmers
	W >= L/4
then
	H = 24 * L


If too much seed has been planted
	S > (6 * L)
and there are too few farmers
	W < L/4
then 
	H = 96 * W	
]]--


local state = {
	grain = {
		food = 0,
		planted = 0,
		reserve = {
			{
				count = 0,           -- quantity of grain
				generationStored = 1 -- when it was stored
			}
		}
	},
	acres = 10,
	population = 1000,
	generation = 1
}

local H = 0                                -- # grain output from harvest
local S = state.grain.planted              -- # grain planted
local L = state.acres                      -- # acres of land cultivated
local W = math.floor(state.population / 3) -- # workers on land (1/3 of population) FIXME: this should also take into account # of available acres




local still_playing = true;

function listener()
	io.write('\n=====\nWhat are your wishes for the kingdom?\n=====\n\n')
	io.write('1. (p)lant grain\n')
	io.write('2. (s)tore grain\n')
	io.write('3. (t)rade grain for more fields\n')
	io.write('4. (l)eave me be! (aka (q)uit the game)\n')
	io.write('\n* ')
	local external_input = io.read()
	return external_input
end


function plant_grain()
	io.write('Plant grain!\n')
	io.write('How much?\n')
	quantity_to_plant = io.read()
	io.write('\nYou planted ' .. quantity_to_plant .. '\n')
end

function store_grain()
	io.write('Store grain!\n')
	io.write('How much?\n')
end

function trade_grain()
	io.write('Trade grain!\n')
	io.write('How much?\n')
end

function clear_screen()
	os.execute("clear") -- FIXME: this won't work across systems, I don't think
end


function process_input(input)
	input = string.lower(input)
	if (input == '1' or input == 'plant' or input == 'p') then
		plant_grain()
	elseif (input == '2' or input == 'store' or input == 's') then
		store_grain()
	elseif (input == '3' or input == 'trade' or input == 't') then
		trade_grain()
	elseif (input == '4' or input == 'leave' or input == 'l' or input == 'quit' or input == 'q' or input == 'exit') then
		still_playing = false
	else
		io.write('\n\nYou are noble and wise for sure -- we don\'t know how to carry out this decree, please, forgive our not knowing what you mean by "' .. input .. '"\n')
	end
end

while (still_playing) do
	local player_input = listener()
	clear_screen()
	process_input(player_input)
end
