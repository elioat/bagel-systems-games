# The Sumerian Game 

More info, 

- <https://en.wikipedia.org/wiki/The_Sumerian_Game>
- <https://adventofcomputing.libsyn.com/episode-57-simulated-sumeria?tdest_id=1264190>
- <https://archive.org/details/ERIC_ED014227/page/n25/mode/1up>
- <https://archive.org/details/ERIC_ED014227/page/n30/mode/1up>

Basic game logic lifted from the above, 


If too much seed has not been planted
	S <= 6 * L
And if there are enough farmers
	W >= L/4
then
	H = 4 * S


If too much seed has not been planted
	S <= 6 * L
but there are too few farmers
	W < L/4
then
	H = (4 * w/L) 4 * S


If too much seed has been planted 
	S > 6 * L
and if there are enough farmers
	W >= L/4
then
	H = 24 * L


If too much seed has been planted
	S > (6 * L)
and there are too few farmers
	W < L/4
then 
	H = 96 * W	