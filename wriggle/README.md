# Turn based RPG?

We'll see!

## Story/structure idea: 

A text based rogue like where you are a Bog Witch, who's just heard a call to a distant planet. To get there you must use your powers to control minor demons and dukes of hell/thralls to prepare you a way to travel to the far off planet. 

Game ends when they make you a way to get to the planet. 

Follow up game takes place on that planet. 

***

Start game by choosing your entrance to hell. Each entrance offers different passive buffs. 

- Satanic pentagram (cliched, little to no buffs)
- Blood sacrifice (what the hell!? dangerous and defo not recommended)
- Knuth's Knishery (if you know the proper thing to order off menu this is a pretty good way in)
- Dentists' convention (make a Faustian bargin. A tooth for passage)
- The Royal Tailor's Tea Emporium (very expensive, but sure fire way in)
- Gerald's House (you know a guy)
- Weir Hill at 23:17 (the right place at the right time)
- Difficult ritual involving the correct number of pigeons (takes a long time, but worth the effort...usually)
- Cursed Shmully's Overlook (creepy as fuck)
- The steaming pits (also cliched, a little warm, but sure fire way in...get it...*fire*)

Once in hell you summon a thrall. 

You then use your thrall, battling and barganing your way to the King of hell. Whenever you defeat an enemy you can choose to enthrall that character, replacing your previous thrall, or you can choose to keep your current and get some passive buff from having won the bout. 

You win the game when you reach the King of hell. There, you are able to make your demand and get a way to the far off planet.

If your thrall is defeated at any point you are ejected from hell and have to start over.

## Mechanics: 

Player must journey through 9 levels of hell. 

At level 9 player wins.


Player state: 

- Health (int, 0 - 100)
- Wealth (int, starts at 0, but can go up or down as necessary)
- Chutzpa (int, 1 - 18)
- Luck (int, 0 - 5)

Health are your hitpoints. When health drops to zero you loose. 

Wealth is a currency you can use to bribe. You bribe to either pass by a thrall, or to get them on to your side.

Chutzpa is the primary stat used to capture a new thrall. Your chutzpa will be tested vs their's. If yours is higher/tied you win. If their's is higher they win. If they win your health is decreased by their attack score.

Luck is a general modifier. It is added to any test you make. So, if you had 5 chutzpa and 2 luck, that is like having 7 chutzpa. 


Enemy/thrall stats: 

- Health, (int, 0 - 50)
- Wealth, (int)
- Chutzpa, (int)
- Attack, (int, 1 - 25)


Player's Health resets on every run. A player's wealth, chutzpa, and luck roll-over from run to run.


Once in the dungeon (hell) you summon a thrall. A random thrall appears and you "convince" it to join you. You convince it by either applying your Chutzpa to it or by bribing it with your wealth. 

Chutzpa is just a straight test, if the player's chutzpa is equal to or greater than the enemy's chutzpa the player wins. If the player looses the enemy's attack is applied to the player's health. 

Once the player defeats an enemy that enemy become's the player's thrall. Now, the player can progress through the dungeon.

The player only ever controls 1 thrall.  

At each level of the dungeon the player and their thrall face 1 or more enemies. 

The player can choose to do 1 of 3 things when faced with an enemy: 

- Attack using their thrall
- Bribe the enemy to let them pass
- Take on the enemy as their new thrall

If the player chooses to attack using their thrall than the thrall faces off against the enemey. First a random "target" number is generated. Next a random "hit" number is generated. If the hit number is equal to or less than the target number than the player's thrall hit's the enemy, applying their attack to the enemy's health. Next the same thing happens again, but this time the enemy is able to attack *either* the player directly or the thrall. After both the thrall and the enemy have gone the bout is complete and the player is left to choose a course of action.

If the player chooses to bribe the enemy to let them pass then a random "target" number is generated. This target number can't be more than the maximum amount of wealth the player has. This target is hidden from the player. Next, the player inputs their offer. They can't offer more than the wealth they have. If the player has offered exactly or more than the target number the bribe is considered to be a success. If the bribe is a success the player's wealth is decreased by their offer PLUS the possibility of a little bit more (maybe determine this with a chutzpa test?). If the player's bribe is rejected the bout is complete and the player is left to choose a course of action. 

If the player chooses to take on the enemy as a new thrall we repease the chutzpa test described above, upon initial entrance into the dungeon. If the player looses that test the bout is considered to be over and the player is left to choose a course of action. If the player wins than they succesfully take on a new thrall and can continue on. (May want to add some consequence to taking on a new thrall? Maybe the old one gets to do 1 attack?)


After the player has progressed through 9 levels of the dungeon they win!