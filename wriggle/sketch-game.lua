lume = require 'lume'
file_utils = require 'file_utils'

local names = lines_from('names.txt')

local player = {
	name = names[lume.round(lume.random(1, #names))],
	health = lume.round(lume.random(87, 175)),
	alive = true,
	power = lume.round(lume.random(42, 100))
}

local still_playing = true;

function listener()
	if (player.alive) then
		io.write('\n=====\nWhat would you like to do?\n=====\n\n')
		io.write('1. (b)attle\n')
		io.write('2. (v)iew stats\n')
		io.write('3. (l)eave\n')
		io.write('\n* ')
	else
		io.write('\nRIP, you are dead. Type l to leave.\n')
		io.write('\n* ')
	end 
	local external_input = io.read()
	return external_input
end


function build_enemy_stats() 
	return {
		name = names[lume.round(lume.random(1, #names))],
		health = lume.round(lume.random(10, 125)),
		alive = true,
		power = lume.round(lume.random(3, 75)),
	}
end

function attack(power, health)
	local attack_damage = lume.round(lume.random(1, power))
	local updated_health = health - attack_damage
	return updated_health
end

function woo(power, health)
	local woo_might = lume.round(lume.random(1, power / 2))
	local updated_health = health + woo_might
	return updated_health
end

function battle()
	local new_enemy = build_enemy_stats()
	io.write('\nENEMY STATS:')
	view_stats(new_enemy)
	io.write('\nYOUR STATS:')
	view_stats(player)
	while new_enemy.alive do
		io.write('\nDo what?\n(a)ttack!\n(w)oo\n* ')
		do_what = io.read()
		if (do_what == 'a') then
			new_enemy.health = attack(player.power, new_enemy.health)
			player.health = attack(new_enemy.power, player.health)
			if (player.health <= 0) then player.alive = false end
			if (new_enemy.health <= 0) then new_enemy.alive = false end
			clear_screen()
			view_stats(new_enemy)
			view_stats(player)
		elseif (do_what == 'w') then
			new_enemy.health = woo(player.power, new_enemy.health)
			local enemy_choice = lume.randomchoice({true, false})
			if (enemy_choice) then
				player.health = attack(new_enemy.power, player.health)
			else
				player.health = woo(new_enemy.power, player.health)
			end
			if (player.health <= 0) then player.alive = false end
			if (new_enemy.health <= 0) then new_enemy.alive = false end
			clear_screen()
			view_stats(new_enemy)
			view_stats(player)
		end
	end
end

function view_stats(stats)
	io.write('\nName: ' .. stats.name)
	io.write('\nHealth: ' .. stats.health)
	io.write('\nPower: ' .. stats.power)
	io.write('\n')
end

function clear_screen()
	os.execute("clear") -- FIXME: this won't work across systems, I don't think
end


function process_input(input)
	input = string.lower(input)
	if (input == '1' or input == 'battle' or input == 'b') then
		battle()
	elseif (input == '2' or input == 'view' or input == 'v') then
		view_stats(player)
	elseif (input == '3' or input == 'leave' or input == 'l' or input == 'quit' or input == 'q' or input == 'exit') then
		still_playing = false
	else
		io.write('\n\nYou are noble and wise for sure -- we don\'t know how to carry out this decree, please, forgive our not knowing what you mean by "' .. input .. '"\n')
	end
end

while (still_playing) do
	local player_input = listener()
	clear_screen()
	process_input(player_input)
end
