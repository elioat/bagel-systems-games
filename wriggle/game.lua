--[[

Global state must track 

- Game status (running vs. quit)
- Current game stage
- Player state

Available game stages

1. Menu
2. Entrance select
3. First encounter
4. Levels in hell
5. End game

From the Menu player can do 1 of the following

- Start a new game
- Load a previous game
- Exit

If the player loads a previous game global state is overwritten with whatever 
is loaded.

If the player opts to start a new game they the game stage progresses from the 
menu to the Entrance select.

After selecting an entrance various buffs and debuffs are applied to the 
player's stats and the game stage rolls over into the 1st encounter! 

]]--


-- INITIAL STATE
local still_playing = true
local game_stage = 1




function menu()
	io.write('\nGame Menu.')
end

function entrance_select()
	io.write('\nPick your entrance!')
end

function first_encounter()
	io.write('\nFirst encounter.')
end

function progress_through_hell()
	io.write('\nTiptoe through the tulips')
end

function end_game()
	io.write('\nThe End.')
end

-- display game UI and prompt for player input
function listener(game_stage)
	if (game_stage) then
		if (game_stage == 1) then     -- menu
			menu()
		elseif (game_stage == 2) then -- entrance select
			entrance_select()
		elseif (game_stage == 3) then -- first encounter
			first_encounter()
		elseif (game_stage == 4) then -- the levels of hell
			progress_through_hell()
		elseif (game_stage == 5) then -- end game
			end_game()
		elseif (game_stage > 5) then
			still_playing = false
		end
	else
		io.write('\nInvalid game stage, ' .. game_stage)
		still_playing = false -- FIXME: die()
	end 
	local external_input = io.read()
	return external_input
end




-- hint at available player actions and act on player input
function process_input(input)
	input = string.lower(input)
	
	if (input == 'q' or input == 'quit' or input == 'exit') then
		still_playing = false
	end

	if (game_stage == 1) then     -- menu

	elseif (game_stage == 2) then -- entrance select

	elseif (game_stage == 3) then -- first encounter

	elseif (game_stage == 4) then -- the levels of hell

	elseif (game_stage == 5) then -- eng game
	
	end
end





function clear_screen()
	os.execute('clear') -- FIXME: seems fragile. Not certain this will work under all operating systems.
end





while (still_playing) do
	local player_input = listener(game_stage)
	clear_screen()
	process_input(player_input)
end
