# Quick and dirty design notes

## Menu,
- New Game
- Resume Game (only present if program sees a previous game save file)
- Quit

## New Game
- Start Adventure (series of encounters using a newly generated character)

## Adventure
- Presented with opponent
- Able to engage, or manage inventory
- Save
- Quit